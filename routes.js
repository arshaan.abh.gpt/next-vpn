var routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "ni ni-tv-2 text-primary",
    layout: "/admin",
  },
  {
    path: "/roll",
    name: "Roll",
    icon: "ni ni-circle-08 text-green",
    layout: "/admin",
  },
  {
    path: "/users",
    name: "Users",
    icon: "ni ni-single-02 text-yellow",
    layout: "/admin",
  },
  {
    path: "/arches",
    name: "Arches",
    icon: "ni ni-align-center text-orange",
    layout: "/admin",
  },
  {
    path: "/packages",
    name: "Packages",
    icon: "ni ni-bullet-list-67 text-red",
    layout: "/admin",
  },
  {
    path: "/charge",
    name: "Charge",
    icon: "ni ni-check-bold text-green",
    layout: "/admin",
  },
  {
    path: "/chargeAdmin",
    name: "Charge Admin",
    icon: "ni ni-check-bold text-teal",
    layout: "/admin",
  },
  {
    path: "/convert",
    name: "Convert",
    icon: "ni ni ni-shop text-red",
    layout: "/admin",
  },
  {
    path: "/exchange",
    name: "Exchange",
    icon: "ni ni-money-coins text-cyan",
    layout: "/admin",
  },
  {
    path: "/vpnList",
    name: "Vpn list",
    icon: "ni ni-lock-circle-open text-indigo",
    layout: "/admin",
  },
  {
    path: "/reports",
    name: "Reports",
    icon: "ni ni-align-center text-purple",
    layout: "/admin",
  },
  {
    path: "/reportsAdmin",
    name: "Reports Admin",
    icon: "ni ni-align-center text-pink",
    layout: "/admin",
  },
  // {
  //   path: "/support",
  //   name: "Support",
  //   icon: "ni ni-support-16 text-blue",
  //   layout: "/admin",
  // },
  {
    path: "/login",
    name: "Login",
    icon: "ni ni-key-25 text-info",
    layout: "/auth",
  },
  {
    path: "/register",
    name: "Register",
    icon: "ni ni-circle-08 text-pink",
    layout: "/auth",
  },
  {
    path: "/forgetPassword",
    name: "Forget Password",
    icon: "ni ni-lock-circle-open text-orange",
    layout: "/auth",
  },
];
export default routes;
